### запустить Docker
    sudo docker-compose up

### остановить контейнеры
    sudo docker-compose stop && docker-compose rm -f
### удалить контейнеры
    sudo docker ps -a | grep "docker-django-angular" | awk '{print $1}' | xargs sudo docker rm


### Зарегистрированные пользователи:
    login: admin, pass: admin 
    login: user1, pass: user1 
    login: user2, pass: user2 


### Админка
    http://localhost:8000/admin


### Тест
    python manage.py test 


## API
### Группы товаров

#### Получить список групп товаров
###### доступно всем авторизованным пользователям
    curl http://localhost:8000/api/product_groups/?format=json

#### Получить группу товаров по ID
###### доступно всем авторизованным пользователям
    curl http://localhost:8000/api/product_groups/<ID>/?format=json
    curl http://localhost:8000/api/product_groups/1/?format=json

#### Добавить группу товаров
###### доступно только пользователям с правами админа
    curl -X POST http://localhost:8000/api/product_groups/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"name": "Мебель"}'

#### Изменить группу товаров по ID
###### доступно только пользователям с правами админа
    curl -X PATCH http://localhost:8000/api/product_groups/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"name": "Корпусная мебель 2"}'

    curl -X PUT http://localhost:8000/api/product_groups/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"name": "Корпусная мебель"}'

#### Удалить группу товаров по ID
###### доступно только пользователям с правами админа
    curl -X DELETE http://localhost:8000/api/product_groups/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin 


### Товары

#### Получить список товаров
###### доступно всем авторизованным пользователям
    curl GET http://localhost:8000/api/products/?format=json

#### Получить список товаров с фильтром по группе
###### доступно всем авторизованным пользователям
    curl GET http://localhost:8000/api/products/?group=<Group ID>&format=json
    curl GET http://localhost:8000/api/products/?group=1&format=json

#### Получить товар по ID
###### доступно всем авторизованным пользователям
    curl GET http://localhost:8000/api/products/<ID>/?format=json
    curl GET http://localhost:8000/api/products/1/?format=json

#### Добавить товар
###### доступно только пользователям с правами админа
    curl -X POST http://localhost:8000/api/products/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"name": "Шкаф", "group": "3"}'

#### Изменить товар по ID
###### доступно только пользователям с правами админа
    curl -X PATCH http://localhost:8000/api/products/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"sku": "RRRR0033", "value": 300}'

    curl -X PUT http://localhost:8000/api/products/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin \
    -d '{"name": "Яблоки", "group": "1", "sku": "RRRR0001", "value": 100}'

#### Удалить группу товар по ID
###### доступно только пользователям с правами админа
    curl -X DELETE http://localhost:8000/api/products/<ID>/ \
    -H "content-type: application/json" \
    -u admin:admin 
