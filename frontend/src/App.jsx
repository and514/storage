import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Route } from 'react-router-dom'

// import  CustomersList from './CustomersList'
// import  CustomerCreateUpdate  from './CustomerCreateUpdate'
import './App.css';
import ProductGroupList from "./components/ProductGroupList/ProductGroupList";

// const BaseLayout = () => (
//
//     <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
//         <div class="container">
//             <a class="navbar-brand" href="/">Storage</a>
//             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-content" aria-controls="navbar-content" aria-expanded="false" aria-label="Toggle navigation">
//                 <span class="navbar-toggler-icon"></span>
//             </button>
//
//             <div class="collapse navbar-collapse" id="navbar-content">
//                 <ul class="navbar-nav mr-auto">
//                     <li class="nav-item"><a class="nav-link" href="/product_groups">Product groups</a></li>
//                     <li class="nav-item"><a class="nav-link" href="/products">Products</a></li>
//                 </ul>
//             </div>
//         </div>
//     </nav>
//
//
//     <div className="content">
//       <Route path="/product_groups" exact component={ProductGroupList} />
//     </div>
//
//
// )

class App extends Component {
  render() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <div className="container">
                <a className="navbar-brand" href="/">Storage</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-content"
                        aria-controls="navbar-content" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbar-content">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item"><a className="nav-link" href="/product_groups">Product groups</a></li>
                        <li className="nav-item"><a className="nav-link" href="/products">Products</a></li>
                    </ul>
                </div>

                      <BrowserRouter>
      <Route path="/product_groups" exact component={ProductGroupList} />
      </BrowserRouter>

            </div>
        </nav>
    );
  }
}


export default App;