from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ModelViewSet

from api.serializers import ProductSerializer, ProductGroupSerializer
from shop.models import ProductGroup, Product


class APIProductGroupViewSet(ModelViewSet):
    queryset = ProductGroup.objects.all()
    serializer_class = ProductGroupSerializer
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly, )


class APIProductViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly, )

    def get_queryset(self):
        queryset = Product.objects.all()
        group = self.request.query_params.get('group')
        if group:
            queryset = queryset.filter(group=group)
        return queryset
