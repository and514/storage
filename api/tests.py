from django.contrib.auth.models import User
from django.forms import model_to_dict
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from shop.models import ProductGroup, Product

ADMIN_CREDENTIALS = {'username': 'adm', 'password': 'adm'}
USER_CREDENTIALS = {'username': 'usr', 'password': 'usr'}
PRODUCT_GROUP_1 = "Product Group 1"
PRODUCT_GROUP_2 = "Product Group 2"
PRODUCT_1 = "Product 1"
PRODUCT_2 = "Product 2"
PRODUCT_1_SKU = "RRRR0001"
PRODUCT_1_VALUE = 20


# noinspection DuplicatedCode
class APIProductGroupTest(APITestCase):
    def setUp(self):
        self.admin = User.objects.create_user(email='adm@example.com', is_superuser=True, **ADMIN_CREDENTIALS)
        self.user = User.objects.create_user(email='usr@example.com', **USER_CREDENTIALS)
        self.admin_client = APIClient()
        self.admin_client.login(**ADMIN_CREDENTIALS)
        self.user_client = APIClient()
        self.user_client.login(**USER_CREDENTIALS)

    def test_product_group(self):
        url = reverse('product_group-list')

        # Create - Admin
        response = self.admin_client.post(url, {'name': PRODUCT_GROUP_1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Create - User
        response = self.user_client.post(url, {'name': PRODUCT_GROUP_2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(ProductGroup.objects.count(), 1)
        product_group = ProductGroup.objects.first()
        self.assertEqual(product_group.name, PRODUCT_GROUP_1)

        url_details = reverse('product_group-detail', args=[product_group.pk])
        # Rename - Admin
        response = self.admin_client.patch(url_details, {'name': PRODUCT_GROUP_2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Rename - User
        response = self.user_client.patch(url_details, {'name': PRODUCT_GROUP_1})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        product_group.refresh_from_db()
        self.assertEqual(product_group.name, PRODUCT_GROUP_2)

        # Get - Admin
        response = self.admin_client.get(url_details, {'pk': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, model_to_dict(product_group))
        # Get - User
        response = self.user_client.get(url_details, {'pk': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, model_to_dict(product_group))

        # Delete - User
        response = self.user_client.delete(url_details, {'pk': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(ProductGroup.objects.count(), 1)
        # Delete - Admin
        response = self.admin_client.delete(url_details, {'pk': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ProductGroup.objects.count(), 0)


# noinspection DuplicatedCode
class APIProductTest(APITestCase):
    def setUp(self):
        self.admin = User.objects.create_user(email='adm@example.com', is_superuser=True, **ADMIN_CREDENTIALS)
        self.user = User.objects.create_user(email='usr@example.com', **USER_CREDENTIALS)
        self.admin_client = APIClient()
        self.admin_client.login(**ADMIN_CREDENTIALS)
        self.user_client = APIClient()
        self.user_client.login(**USER_CREDENTIALS)

    def test_product_group(self):
        url = reverse('product-list')
        url_group = reverse('product_group-list')

        # Create Group
        response = self.admin_client.post(url_group, {'name': PRODUCT_GROUP_1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        product_group = ProductGroup.objects.first()

        # Create - Admin
        response = self.admin_client.post(url, {'name': PRODUCT_1, 'group': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Create - User
        response = self.user_client.post(url, {'name': PRODUCT_2, 'group': product_group.pk})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(Product.objects.count(), 1)
        product = Product.objects.select_related('group').first()
        self.assertEqual(product.name, PRODUCT_1)
        self.assertEqual(product.group.name, PRODUCT_GROUP_1)
        self.assertFalse(product.sku)
        self.assertFalse(product.value)

        url_details = reverse('product-detail', args=[product.pk])
        # Update - User
        response = self.user_client.patch(url_details, {'sku': PRODUCT_1_SKU, 'value': PRODUCT_1_VALUE})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Update - Admin
        response = self.admin_client.patch(url_details, {'sku': PRODUCT_1_SKU, 'value': PRODUCT_1_VALUE})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        product.refresh_from_db()
        self.assertEqual(product.sku, PRODUCT_1_SKU)
        self.assertEqual(product.value, PRODUCT_1_VALUE)

        # Get - Admin
        response = self.admin_client.get(url_details, {'pk': product.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, model_to_dict(product))
        # Get - User
        response = self.user_client.get(url_details, {'pk': product.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, model_to_dict(product))

        # Delete - User
        response = self.user_client.delete(url_details, {'pk': product.pk})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Product.objects.count(), 1)
        # Delete - Admin
        response = self.admin_client.delete(url_details, {'pk': product.pk})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 0)
