from rest_framework.routers import DefaultRouter

from api.views import APIProductViewSet, APIProductGroupViewSet

api_router = DefaultRouter()
api_router.register('products', APIProductViewSet, basename='product', )
api_router.register('product_groups', APIProductGroupViewSet, basename='product_group')
