from django.contrib import admin

from .models import ProductGroup, Product


admin.site.register(ProductGroup)
admin.site.register(Product)
