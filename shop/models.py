from django.db import models
from django.utils.translation import ugettext_lazy as _


class ProductGroup(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=250, null=False, blank=False, db_index=True)

    class Meta:
        verbose_name = _('Product group')
        verbose_name_plural = _('Product groups')
        ordering = ['name']

    def __str__(self):
        return f'[{self.id}] {self.name}'
        

class Product(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=250, null=False, blank=False, db_index=True)
    group = models.ForeignKey(ProductGroup, null=False, related_name='products', on_delete=models.PROTECT)
    sku = models.CharField(verbose_name=_('SKU'), max_length=8, null=False, blank=False, default='', db_index=True)
    value = models.IntegerField(verbose_name=_('Amount'), default=0)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ['name']

    def __str__(self):
        return f'[{self.id}] {self.sku}, {self.name}, {self.group} - {self.value}'
