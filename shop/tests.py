from django.test import TestCase

from .models import ProductGroup, Product


gr1_name = 'Посуда'
gr2_name = 'Строительные материалы'

pr1_name = 'Тарелка'
pr2_name = 'Кастрюля'
pr3_name = 'Чашка'


# noinspection DuplicatedCode
class ProductGroupModelTest(TestCase):
    def setUp(self):
        ProductGroup.objects.create(name=gr1_name)
        ProductGroup.objects.create(name=gr2_name)
        
    def test_product_group(self):
        gr1 = ProductGroup.objects.get(name=gr1_name)
        gr2 = ProductGroup.objects.get(name=gr2_name)
        self.assertEqual(gr1.name, gr1_name)
        self.assertEqual(gr2.name, gr2_name)
        self.assertEqual(ProductGroup.objects.count(), 2)
        gr1.delete()
        self.assertEqual(ProductGroup.objects.count(), 1)


# noinspection DuplicatedCode
class ProductModelTest(TestCase):
    def setUp(self):
        self.gr1 = ProductGroup.objects.create(name=gr1_name)
        Product.objects.create(name=pr1_name, group=self.gr1, sku='RRRR0001', value=54)
        Product.objects.create(name=pr2_name, group=self.gr1)

    def test_product(self):
        pr1 = Product.objects.get(name=pr1_name)
        pr2 = Product.objects.get(name=pr2_name)
        self.assertEqual(pr1.name, pr1_name)
        self.assertEqual(pr2.name, pr2_name)
        self.assertEqual(Product.objects.count(), 2)
        pr1.delete()
        self.assertEqual(Product.objects.count(), 1)
        # Delete group - exception
        with self.assertRaises(Exception):
            self.gr1.delete()
        # Create without group - exception
        with self.assertRaises(Exception):
            Product.objects.create(name=pr3_name)
